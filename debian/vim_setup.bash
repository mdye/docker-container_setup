#!/usr/bin/env bash

apt-get update && apt-get install -y vim

${BASH_SOURCE%/*}/../common/vim_setup.bash
