#!/usr/bin/env bash

apt-get update && apt-get install -y openssh-server

${BASH_SOURCE%/*}/../common/ssh_setup.bash
