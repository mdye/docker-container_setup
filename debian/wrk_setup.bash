#!/usr/bin/env bash

apt-get update && apt-get install -y git build-essential libssl-dev

${BASH_SOURCE%/*}/../common/wrk_setup.bash
