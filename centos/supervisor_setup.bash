#!/usr/bin/env bash

yum install -y python-pip && pip install supervisor && mkdir -p /var/log/supervisor

cat > /etc/supervisord.conf <<EOF
[supervisord]
nodaemon=true
logfile=/var/log/supervisor/supervisord.log ; (main log file;default $CWD/supervisord.log)
pidfile=/var/run/supervisord.pid ; (supervisord pidfile;default supervisord.pid)
childlogdir=/var/log/supervisor            ; ('AUTO' child log dir, default $TEMP)

[inet_http_server]
port = 127.0.0.1:9001
username = root
password = root

[supervisorctl]
serverurl = http://127.0.0.1:9001
username = root
password = root
prompt = supervisorctl

[rpcinterface:supervisor]
supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface
EOF
