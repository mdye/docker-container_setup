#!/usr/bin/env bash

RPM="${BASH_SOURCE%/*}/jdk.rpm"

curl -sSf -L --cookie "oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u25-b17/jdk-8u25-linux-x64.rpm > $RPM
rpm -ivh $RPM
echo -e 'export JAVA_HOME=/usr/java/latest\nexport PATH=$JAVA_HOME/bin:$PATH' >> /root/.profile
