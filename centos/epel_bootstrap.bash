#!/usr/bin/env bash

cat > /etc/yum.repos.d/epel-bootstrap.repo <<EOF
[epel-bootstrap]
name=EPEL Bootstrap
baseurl=http://dl.fedoraproject.org/pub/epel/7/x86_64/
enabled=0
gpgcheck=0
EOF

yum --enablerepo='epel-bootstrap' -y install epel-release
