#!/usr/bin/env bash

yum install -y openssh-server

# must remove pam_loginuid for ssh to work on some versions of centos
sed -ri 's|session.*required.*pam_loginuid.so|session optional pam_loginuid.so|' /etc/pam.d/sshd

${BASH_SOURCE%/*}/../common/ssh_setup.bash
