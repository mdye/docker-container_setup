#!/usr/bin/env bash

yum install -y vim

${BASH_SOURCE%/*}/../common/vim_setup.bash

# hose that silly root exception for vi alias in /etc/profile.d/vim.sh
sed -ri '/[ -n "$ID" -a "$ID" -le 200 ] && return/d' /etc/profile.d/vim.sh
