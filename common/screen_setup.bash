#!/usr/bin/env bash

cat > /root/.screenrc <<EOF
  #password
  vbell off                     # default: off
  vbell_msg "                   -- Ring, Ring!! --   "  # default: "Wuff,Wuff!!"
  autodetach on                 # default: on
  startup_message off           # default: on
  defscrollback 10000           # default: 100
  caption always "%{=}%?% %L=%{+b}%?%{b}%-Lw%47L>%?%{r}%n %t [%h]%{b}%+Lw%?%{g}%-31=%c %l %Y-%m-%d"

  attrcolor b ".I" # allow bold colors
  termcapinfo xterm 'Co#256:AB=\E[48;5;%dm:AF=\E[38;5;%dm' # AB=background, AF=foreground
  defbce "on" # erase background with current bg color
EOF
