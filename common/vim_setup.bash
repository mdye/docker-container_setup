#!/usr/bin/env bash

cat > /root/.vimrc <<EOF
set tabstop=2
set shiftwidth=2
set expandtab
set ignorecase
set showmatch
EOF
