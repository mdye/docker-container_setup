#!/usr/bin/env bash

sed -ri 's/PermitRootLogin .*/PermitRootLogin yes/g' /etc/ssh/sshd_config
sed -ri 's/PubkeyAuthentication no/PubkeyAuthentication yes/g' /etc/ssh/sshd_config
sed -ri 's/RSAAuthentication no/RSAAuthentication yes/g' /etc/ssh/sshd_config
sed -ri 's|#AuthorizedKeysFile.*$|AuthorizedKeysFile\  \.ssh/authorized_keys|g' /etc/ssh/sshd_config
mkdir -p /var/run/sshd
chmod -rx /var/run/sshd
ssh-keygen -N '' -t rsa -f /etc/ssh/ssh_host_rsa_key
mkdir /root/.ssh/

cat >> /etc/supervisord.conf <<EOF

[program:sshd]
command=/usr/sbin/sshd -D
autostart=true
autorestart=true
stderr_logfile=/var/log/sshd.err.log
stdout_logfile=/var/log/sshd.out.log
EOF
