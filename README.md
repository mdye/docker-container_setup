## VCS-managed Docker container setup scripts

### Usage

In Dockerfile, install `git`, clone this repo (usually in `/container_setup`), and execute scripts in `/container_setup/<distro_name>/`. For example, if using CentOS and setting up SSH, add commands like these to a Dockerfile:

        RUN yum install -y git
        RUN cd / && git clone https://bitbucket.org/mdye/docker-container_setup.git
        RUN /docker-container_setup/centos/ssh_setup.bash

Note that distribution-specific setup scripts often delegate to scripts in `common/` to perform steps that are the same between distributions.
